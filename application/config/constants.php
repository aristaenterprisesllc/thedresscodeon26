<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Site Resources Paths
|--------------------------------------------------------------------------
|
| Here we keep the paths for images, javascripts and css
| MAKSumon - August 26, 2013
*/
define('BASE_PATH', 'http://localhost/thedresscodeon26/');
//define('BASE_PATH', 'http://192.168.0.12/thedresscodeon26/');
//define('BASE_PATH', 'http://www.aristaenterprise.com/thedresscodeon26/');
//define('BASE_PATH', 'http://arista.dlinkddns.com/thedresscodeon26/');
//define('BASE_PATH', 'http://thedresscodeon26.com/testsite/');
define('NAV_PATH', BASE_PATH.'index.php/');
define('IMAGES_PATH', BASE_PATH.'resources/images/');
define('JS_PATH', BASE_PATH.'resources/js/');
define('CSS_PATH', BASE_PATH.'resources/css/');
define('FLASH_PATH', BASE_PATH.'resources/flash/');
define('PDF_PATH', BASE_PATH.'resources/pdfs/');

/*
 |--------------------------
 | Messages
 |--------------------------
 */
// message shown after value saved sucessfully
define('DATA_SAVED_SUCCESSFULLY_MSG', 'Data saved successfully');
define('DATA_SAVED_UNSUCCESSFULLY_MSG', 'Error occured in data saving');
define('DATA_DELETED_SUCCESSFULLY_MSG', 'Data deleted successfully');
define('DATA_DELETED_UNSUCCESSFULLY_MSG', 'Error occured in data deleting');

/* login remember me start */
define('COOKIE_NAME', 'arista_rememberme');
define('COOKIE_EXPIRATION_TIME', '604800');
/* login remember me end */

/* default time zone start */
define('TIMEZONE_VALUE', 'UTC');
/* default time zone end */

/* Operating System start */
define('IS_WIN_OS', 0);
/* Operating System end */

/* westlake admin email address start */
/* from Guest Book and Contact Us, information will be sent to this address */
define('ADMIN_EMAIL_TO', 'mkabir@aristaenterprise.com');
define('ADMIN_EMAIL_TO_TEST', 'msalim@aristaenterprise.com');
/* westlake admin email address end */

/* from Guest Book and Contact Us, information will be sent from this address */
define('ADMIN_EMAIL_FROM', 'info@aristaenterprise.com');


/* End of file constants.php */
/* Location: ./application/config/constants.php */