<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class About_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {
        $this->load->model('about_model', 'am', TRUE);
        $result = $this->am->select_all_from_about();
        
        $this->load->model('social_model', 'sm', TRUE);
        $social = $this->sm->select_all_from_social();
        
        $data = array();
        $data['dbdata'] = $result;
        $data['social'] = $social;

        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/about_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    function updateValue() {
        $this->load->model('about_model', 'am', TRUE);
        $data = array();
        $str_this_page_url = NAV_PATH.'admins/about_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('confirm')) {
            try {
                $data['title'] = trim($this->input->post('title'));
                $data['top_title'] = trim($this->input->post('top_title'));
                $data['top_details'] = trim($this->input->post('top_details'));
                $data['middle_title'] = trim($this->input->post('middle_title'));
                $data['middle_details'] = trim($this->input->post('middle_details'));
                $data['bottom_title'] = trim($this->input->post('bottom_title'));
                $data['bottom_details'] = trim($this->input->post('bottom_details'));

                $query_result = $this->am->update_about($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
                redirect($str_this_page_url,'refresh');
            }
            catch(Exception $ez) {
                redirect($str_this_page_url,'refresh');
            }
        }
    }
}

?>
