<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class Contact_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {
        $this->load->model('contact_model', 'cm', TRUE);
        $result = $this->cm->select_all_from_contact();
        
        $this->load->model('social_model', 'sm', TRUE);
        $social = $this->sm->select_all_from_social();
        
        $data = array();
        $data['dbdata'] = $result;
        $data['social'] = $social;
        
        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/contact_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    function updateValue() {
        $this->load->model('contact_model', 'cm', TRUE);
        $data = array();
        $str_this_page_url = NAV_PATH.'admins/contact_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('confirm')) {
            try {
            	$data['title'] = trim($this->input->post('title'));
            	$data['subtitle'] = trim($this->input->post('subtitle'));
            	$data['location_details'] = trim($this->input->post('location_details'));
            	$data['email_details'] = trim($this->input->post('email_details'));
                $data['call_details'] = trim($this->input->post('call_details'));
                $data['fax_details'] = trim($this->input->post('fax_details'));
                $data['business_hours_details'] = trim($this->input->post('business_hours_details'));

                $query_result = $this->cm->update_contact($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
                redirect($str_this_page_url,'refresh');
            }
            catch(Exception $ez) {
                redirect($str_this_page_url,'refresh');
            }
        }
    }
}

?>
