<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class Login_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {	
    	$this->load->model('social_model', 'sm', TRUE);
    	$social = $this->sm->select_all_from_social();
    	
    	$data = array();
    	$data['social'] = $social;
    	
        // check if logged in
        if($this->session->userdata('logged_in')) {
            // logged in
            $this->load->view('admins/header_admin_view',$data);
            $this->load->view('admins/admin_menu_view');
            $this->load->view('footer_view');
        } else {
            $this->load->view('admins/header_admin_view',$data);
            $this->load->view('admins/login_admin_view');
            $this->load->view('footer_view');
        }
    }
	
    public function checkLogin(){

        $str_this_page_url = NAV_PATH.'admins/login_admin';

        if($this->input->post('login')) {
            try {
                $username = trim($this->input->post('username'));
                $password = trim($this->input->post('password'));

                if($this->simpleloginsecure->login($username, $password)) {
                        // success
                        redirect($str_this_page_url,'refresh');
                } else {
                        // failure
                        redirect($str_this_page_url,'refresh');
                }
            }
            catch(Exception $ez) {
                redirect($str_this_page_url,'refresh');
            }
        }
    }

    public function logout(){

            // logout
            $this->simpleloginsecure->logout();
            redirect(base_url(),'refresh');
    }
}

?>
