<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class Products_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {
        $this->load->model('products_model', 'pm', TRUE);
        $result = $this->pm->select_all_from_products();
        
        $this->load->model('social_model', 'sm', TRUE);
        $social = $this->sm->select_all_from_social();
        
        $data = array();
        $data['dbdata'] = $result;
        $data['social'] = $social;

        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/products_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    public function updateView($id)
    {
    	$this->load->model('products_model', 'pm', TRUE);
    	$result = $this->pm->select_from_products($id);
    
    	$data = array();
    	$data['dbdata'] = $result;
    
    	// check if logged in
    	if($this->session->userdata('logged_in')) {
    		// logged in
    		$this->load->view('admins/header_admin_view',$data);
    		$this->load->view('admins/products_edit_view');
    		$this->load->view('footer_view');
    	} else {
    		$this->load->view('admins/header_admin_view');
    		$this->load->view('admins/login_admin_view');
    		$this->load->view('footer_view');
    	}
    }
    
	function insertProduct() {
        $this->load->model('products_model', 'pm', TRUE);
        
        $data = array();
        
        $str_this_page_url = NAV_PATH.'admins/products_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('confirm')) {
        	
        	$data['name'] = trim($this->input->post('name'));
        	$data['details'] = trim($this->input->post('details'));
            
            $config['upload_path'] = './resources/images/';
            $config['allowed_types'] = 'jpg';
            $config['max_size']	= '';
            $config['max_width']  = '';
            $config['max_height']  = '';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect($str_this_page_url);
            } else {
                $upload_data = $this->upload->data();
                $data['imageName'] = $upload_data['file_name'];
                
                $query_result = $this->pm->insert_product($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
            }
            redirect($str_this_page_url,'refresh');
        }
    }
    
    function updateProduct($id) {
    	$this->load->model('products_model', 'pm', TRUE);
    	$data = array();
    	$str_this_page_url = NAV_PATH.'admins/products_admin';
    
    	$this->session->set_flashdata('message', '');
    
    	//if($this->input->post('update')) {
    		$data['name'] = trim($this->input->post('name'));
        	$data['details'] = trim($this->input->post('details'));
        	
        	$config['upload_path'] = './resources/images/';
        	$config['allowed_types'] = 'jpg';
        	$config['max_size']	= '';
        	$config['max_width']  = '';
        	$config['max_height']  = '';
        	$config['overwrite'] = TRUE;
        	
        	$this->load->library('upload', $config);
        	
        	if (strlen($_FILES['image']['name']) > 0){
        		if (!$this->upload->do_upload('image')) {
        			$this->session->set_flashdata('message', $this->upload->display_errors());
        			redirect($str_this_page_url);
        		} else {
        			$upload_data = $this->upload->data();
        			$data['imageName'] = $upload_data['file_name'];
        			 
        			$query_result = $this->pm->update_product($id,$data);
        			 
        			if($query_result) { // data update successful
        				$this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
        			}
        			else { // error in data update
        				$this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
        			}
        		}
        	} else {
        		$query_result = $this->pm->update_product($id,$data);
        		
        		if($query_result) { // data update successful
        			$this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
        		}
        		else { // error in data update
        			$this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
        		}
        	}
    	//}
    
    	redirect($str_this_page_url,'refresh');
    }
    
    function deleteProduct($id,$fileName) {
        $this->load->model('products_model', 'pm', TRUE);
        
        $str_this_page_url = NAV_PATH.'admins/products_admin';

        $this->session->set_flashdata('message', '');
        
        //if($this->input->post('delete')) {            
            if($this->pm->delete_product($id)) { // data delete successful                
                $path_to_file = './resources/images/'.$fileName;
                
                if(unlink($path_to_file)) {
                    $this->session->set_flashdata('message', DATA_DELETED_SUCCESSFULLY_MSG);
                }
                else {
                   $this->session->set_flashdata('message', DATA_DELETED_UNSUCCESSFULLY_MSG);
                }
            }
            else { // error in data delete
                $this->session->set_flashdata('message', DATA_DELETED_UNSUCCESSFULLY_MSG);
            }
        //}
        
        redirect($str_this_page_url,'refresh');
    }
}

?>
