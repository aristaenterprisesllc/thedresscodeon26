<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class Services_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {
        $this->load->model('services_model', 'sm', TRUE);
        $result = $this->sm->select_all_from_services();
        
        $this->load->model('social_model', 'som', TRUE);
        $social = $this->som->select_all_from_social();
        
        $data = array();
        $data['dbdata'] = $result;
        $data['social'] = $social;

        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/services_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    function updateValue() {
        $this->load->model('services_model', 'sm', TRUE);
        $data = array();
        $str_this_page_url = NAV_PATH.'admins/services_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('confirm')) {
            try {
                $data['title'] = trim($this->input->post('title'));
                $data['details'] = trim($this->input->post('details'));
                $data['middle_left_title'] = trim($this->input->post('middle_left_title'));
                $data['middle_left_details'] = trim($this->input->post('middle_left_details'));
                $data['middle_center_title'] = trim($this->input->post('middle_center_title'));
                $data['middle_center_details'] = trim($this->input->post('middle_center_details'));
                $data['middle_right_title'] = trim($this->input->post('middle_right_title'));
                $data['middle_right_details'] = trim($this->input->post('middle_right_details'));
                $data['bottom_details'] = trim($this->input->post('bottom_details'));

                $query_result = $this->sm->update_services($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
                redirect($str_this_page_url,'refresh');
            }
            catch(Exception $ez) {
                redirect($str_this_page_url,'refresh');
            }
        }
    }
    
    function uploadImage($columnName){
    	$this->load->model('services_model', 'sm', TRUE);

    	$data = array();

    	$str_this_page_url = NAV_PATH.'admins/services_admin';
    	
    	$this->session->set_flashdata('message', '');
    	
    	if($this->input->post('upload')) {
    	
    		$config['upload_path'] = './resources/images/';
    		$config['allowed_types'] = 'jpg';
    		$config['max_size']	= '';
    		$config['max_width']  = '';
    		$config['max_height']  = '';
    		$config['overwrite'] = TRUE;
    	
    		$this->load->library('upload', $config);
    	
    		if (!$this->upload->do_upload($columnName)) {
    			$this->session->set_flashdata('message', $this->upload->display_errors());
    			redirect($str_this_page_url);
    		} else {
    			$upload_data = $this->upload->data();
    			$data[$columnName] = $upload_data['file_name'];
    	
    			$query_result = $this->sm->update_services($data);
    	
    			if($query_result) { // data update successful
    				$this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
    			}
    			else { // error in data update
    				$this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
    			}
    		}
    		redirect($str_this_page_url,'refresh');
    	}
    }
}

?>
