<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class SlideShow_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }
    
    public function index()
    {   
        $this->load->model('slideshow_model', 'sm', TRUE);
		$centerSlide = $this->sm->select_all_from_slideshow('centerSlide');
		$rightTopSlide = $this->sm->select_all_from_slideshow('rightTopSlide');
		$rightBottomSlide = $this->sm->select_all_from_slideshow('rightBottomSlide');
		
		$this->load->model('social_model', 'sm', TRUE);
		$social = $this->sm->select_all_from_social();
        
        $data = array();
        $data['centerSlide'] = $centerSlide;
		$data['rightTopSlide'] = $rightTopSlide;
		$data['rightBottomSlide'] = $rightBottomSlide;
		$data['social'] = $social;

        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/slideshow_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    function insertImage($slideName) {
        $this->load->model('slideshow_model', 'sm', TRUE);
        
        $data = array();
        $data['slideName'] = $slideName;
        
        $str_this_page_url = NAV_PATH.'admins/slideshow_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('upload')) {
            
            $config['upload_path'] = './resources/images/';
            $config['allowed_types'] = 'jpg';
            $config['max_size']	= '';
            $config['max_width']  = '';
            $config['max_height']  = '';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('slideShow')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect($str_this_page_url);
            } else {
                $upload_data = $this->upload->data();
                $data['fileName'] = $upload_data['file_name'];
                
                $query_result = $this->sm->insert_imageURLs($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
            }
            redirect($str_this_page_url,'refresh');
        }
    }
    
    function deleteImage($id,$fileName) {
        $this->load->model('slideshow_model', 'sm', TRUE);
        
        $str_this_page_url = NAV_PATH.'admins/slideshow_admin';

        $this->session->set_flashdata('message', '');
        
        //if($this->input->post('delete')) {            
            if($this->sm->delete_imageURLs($id)) { // data delete successful                
                $path_to_file = './resources/images/'.$fileName;
                
                if(unlink($path_to_file)) {
                    $this->session->set_flashdata('message', DATA_DELETED_SUCCESSFULLY_MSG);
                }
                else {
                   $this->session->set_flashdata('message', DATA_DELETED_UNSUCCESSFULLY_MSG);
                }
            }
            else { // error in data delete
                $this->session->set_flashdata('message', DATA_DELETED_UNSUCCESSFULLY_MSG);
            }
        //}
        
        redirect($str_this_page_url,'refresh');
    }
}

?>
