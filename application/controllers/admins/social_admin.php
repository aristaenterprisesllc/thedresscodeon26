<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_admin
 *
 * @author MAKSumon
 */
class Social_Admin extends CI_Controller {

    function __construct()
    {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('SimpleLoginSecure');
    }
    
    public function index()
    {
        $this->load->model('social_model', 'sm', TRUE);
        $result = $this->sm->select_all_from_social();
        
        $data = array();
        $data['social'] = $result;

        // check if logged in
        if($this->session->userdata('logged_in')) {
                // logged in
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/social_admin_view');
                $this->load->view('footer_view');
        } else {
                $this->load->view('admins/header_admin_view',$data);
                $this->load->view('admins/login_admin_view');
                $this->load->view('footer_view');
        }
    }
    
    function updateValue() {
        $this->load->model('social_model', 'sm', TRUE);
        $data = array();
        $str_this_page_url = NAV_PATH.'admins/social_admin';

        $this->session->set_flashdata('message', '');
        
        if($this->input->post('confirm')) {
            try {
                $data['google'] = trim($this->input->post('google'));
                $data['twitter'] = trim($this->input->post('twitter'));
                $data['facebook'] = trim($this->input->post('facebook'));

                $query_result = $this->sm->update_social($data);

                if($query_result) { // data update successful
                    $this->session->set_flashdata('message', DATA_SAVED_SUCCESSFULLY_MSG);
                }
                else { // error in data update
                    $this->session->set_flashdata('message', DATA_SAVED_UNSUCCESSFULLY_MSG);
                }
                redirect($str_this_page_url,'refresh');
            }
            catch(Exception $ez) {
                redirect($str_this_page_url,'refresh');
            }
        }
    }
}

?>
