<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('products_model', 'pm', TRUE);
		$result = $this->pm->select_all_from_products();
		
		$this->load->model('welcome_model', 'wm', TRUE);
		$welcome = $this->wm->select_all_from_welcome();
		
		$this->load->model('social_model', 'sm', TRUE);
		$social = $this->sm->select_all_from_social();
		
        $data = array();
        
        $data['current'] = "products";
        $data['dbdata'] = $result;
        $data['title'] = $welcome;
        $data['social'] = $social;
            
        $this->load->view('header_view',$data);
        $this->load->view('products_view');
        $this->load->view('footer_view');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */