<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->load->model('services_model', 'sm', TRUE);
		$result = $this->sm->select_all_from_services();
		
		$this->load->model('appointment_model', 'am', TRUE);
		$appointment = $this->am->select_all_from_appointment();
		
		$this->load->model('social_model', 'som', TRUE);
		$social = $this->som->select_all_from_social();
            
		$data = array();
        
        $data['current'] = "services";
        $data['dbdata'] = $result;
        $data['appointment'] = $appointment;
        $data['social'] = $social;
            
        $this->load->view('header_view',$data);
        $this->load->view('services_view');
        $this->load->view('footer_view');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */