<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model('welcome_model', 'wm', TRUE);
		$result = $this->wm->select_all_from_welcome();
		
		$this->load->model('appointment_model', 'am', TRUE);
		$appointment = $this->am->select_all_from_appointment();
		
		$this->load->model('services_model', 'sm', TRUE);
		$services = $this->sm->select_all_from_services();
		
		$this->load->model('slideshow_model', 'slm', TRUE);
		$centerSlide = $this->slm->select_all_from_slideshow('centerSlide');
		$rightTopSlide = $this->slm->select_all_from_slideshow('rightTopSlide');
		$rightBottomSlide = $this->slm->select_all_from_slideshow('rightBottomSlide');
		
		$this->load->model('social_model', 'som', TRUE);
		$social = $this->som->select_all_from_social();
		
		$data = array();
		
		$data['current'] = "home";
		$data['dbdata'] = $result;
		$data['appointment'] = $appointment;
		$data['services'] = $services;
		$data['centerSlide'] = $centerSlide;
		$data['rightTopSlide'] = $rightTopSlide;
		$data['rightBottomSlide'] = $rightBottomSlide;
		$data['social'] = $social;
            
        $this->load->view('header_view',$data);
        $this->load->view('welcome_view');
        $this->load->view('footer_view');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */