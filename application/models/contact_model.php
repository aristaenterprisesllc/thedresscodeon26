<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_model
 *
 * @author MAKSumon
 */
class Contact_Model extends CI_Model {
    
    function select_all_from_contact() {

        try {
            $this->db->select('*');
            $this->db->from('contact');
            $query=$this->db->get();
            $query_result = $query->result_array();

            if(count($query_result)) {
                return $query_result[0];
            }
        }
        catch(Exception $e) {
            return false;
        }
    }
    
    function update_contact($arr_data) {
        try {
        	$this->db->where('id', 1);
            $this->db->update('contact', $arr_data);
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }
}

?>
