<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_model
 *
 * @author MAKSumon
 */
class Products_Model extends CI_Model {
    
    function select_all_from_products() {

        try {
            $this->db->select('*');
            $this->db->from('products');
            $query=$this->db->get();
            $query_result = $query->result_array();

            if(count($query_result)) {
                return $query_result;
            }
        }
        catch(Exception $e) {
            return false;
        }
    }
    
    function select_from_products($id) {
    
    	try {
    		$this->db->select('*');
    		$this->db->from('products');
    		$this->db->where('id',$id);
    		$query=$this->db->get();
    		$query_result = $query->result_array();
    
    		if(count($query_result)) {
    			return $query_result[0];
    		}
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
    
	function insert_product($data) {
    	try {
    		$this->db->insert('products', $data);
    		return true;
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
    function update_product($id,$data) {
    	try {
    		$this->db->where('id', $id);
    		$this->db->update('products', $data);
    		return true;
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
    
    function delete_product($id) {
    	try {
    		$this->db->where('id', $id);
    		$this->db->delete('products');
    		return true;
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
}

?>
