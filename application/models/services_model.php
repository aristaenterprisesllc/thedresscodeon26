<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_model
 *
 * @author MAKSumon
 */
class Services_Model extends CI_Model {
    
    function select_all_from_services() {

        try {
            $this->db->select('*');
            $this->db->from('services');
            $query=$this->db->get();
            $query_result = $query->result_array();

            if(count($query_result)) {
                return $query_result[0];
            }
        }
        catch(Exception $e) {
            return false;
        }
    }
    
    function update_services($arr_data) {
        try {
        	$this->db->where('id', 1);
            $this->db->update('services', $arr_data);
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }
}

?>
