<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_model
 *
 * @author MAKSumon
 */
class Slideshow_Model extends CI_Model {
	
	function select_all_from_slideshow($slideName) {
	
		try {
			$this->db->select('*');
			$this->db->from('slideshow');
			$this->db->where('slideName',$slideName);
			$this->db->order_by("id","asc");
			$query=$this->db->get();
			$query_result = $query->result_array();
	
			if(count($query_result)) {
				return $query_result;
			}
		}
		catch(Exception $e) {
			return false;
		}
	}
    
    function insert_imageURLs($data) {
    	try {
    		$this->db->insert('slideshow', $data);
    		return true;
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
    
    function delete_imageURLs($id) {
    	try {
    		$this->db->where('id', $id);
    		$this->db->delete('slideshow');
    		return true;
    	}
    	catch(Exception $e) {
    		return false;
    	}
    }
}

?>
