<div id="body">
	<div id="featured">
		<h3><?php echo $dbdata['title']; ?></h3>
	</div>
	<div id="content">
		<div id="about">
			<h3><?php echo $dbdata['top_title']; ?></h3>
			<p><?php echo $dbdata['top_details']; ?></p>
			<h3><?php echo $dbdata['middle_title']; ?></h3>
			<p><?php echo $dbdata['middle_details']; ?></p>
			<h3><?php echo $dbdata['bottom_title']; ?></h3>
			<p><?php echo $dbdata['bottom_details']; ?></p>
		</div>
	</div>
</div>