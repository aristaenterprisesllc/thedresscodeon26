<div id="body">
	<div id="featured">
		<h3>About Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/about_admin/updateValue/'); ?>
                <p>Title</p>
                <input type="text" name="title" size="65" value="<?php echo $dbdata['title']; ?>" />
                <p>Top Title</p>
                <input type="text" name="top_title" size="65" value="<?php echo $dbdata['top_title']; ?>" />
                <p>Top Details</p>
                <textarea name="top_details" class="customTextArea">
                    <?php echo $dbdata['top_details']; ?>
                </textarea>
                <p>Middle Title</p>
                <input type="text" name="middle_title" size="65" value="<?php echo $dbdata['middle_title']; ?>" />
                <p>Middle Details</p>
                <textarea name="middle_details" class="customTextArea">
                    <?php echo $dbdata['middle_details']; ?>
                </textarea>
                <p>Bottom Title</p>
                <input type="text" name="bottom_title" size="65" value="<?php echo $dbdata['bottom_title']; ?>" />
                <p>Bottom Details</p>
                <textarea name="bottom_details" class="customTextArea">
                    <?php echo $dbdata['bottom_details']; ?>
                </textarea>
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>