<div id="body">
	<div id="featured">
		<h3>Admin Menu</h3>
	</div>
	<div id="content">
		<div id="admin">
           	<ul>
            	<li><a href="<?php echo NAV_PATH ?>admins/welcome_admin">Welcome Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/products_admin">Products Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/services_admin">Services Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/about_admin">About Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/contact_admin">Contact Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/appointment_admin">Appointment Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/slideshow_admin">Slide Show Configuration</a></li>
                <li><a href="<?php echo NAV_PATH ?>admins/social_admin">Social Link Configuration</a></li>
          	</ul>
		</div>
	</div>
</div>
