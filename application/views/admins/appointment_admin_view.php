<div id="body">
	<div id="featured">
		<h3>Appointment Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/appointment_admin/updateValue/'); ?>
                <p>Call</p>
                <input type="text" name="call_details" size="65" value="<?php echo $dbdata['call_details']; ?>" />
                <p>Fax</p>
                <input type="text" name="fax_details" size="65" value="<?php echo $dbdata['fax_details']; ?>" />
                <p>Email</p>
                <textarea name="email_details" class="customTextArea">
                    <?php echo $dbdata['email_details']; ?>
                </textarea>
                <p>Location</p>
                <textarea name="location_details" class="customTextArea">
                    <?php echo $dbdata['location_details']; ?>
                </textarea>
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>