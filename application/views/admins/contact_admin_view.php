<div id="body">
	<div id="featured">
		<h3>Contact Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/contact_admin/updateValue/'); ?>
                <p>Title</p>
                <input type="text" name="title" size="85" value="<?php echo $dbdata['title']; ?>" />
                <p>Subtitle</p>
                <textarea name="subtitle" class="customTextArea">
                    <?php echo $dbdata['subtitle']; ?>
                </textarea>
                <p>Location</p>
                <textarea name="location_details" class="customTextArea">
                    <?php echo $dbdata['location_details']; ?>
                </textarea>
                <p>Email</p>
                <textarea name="email_details" class="customTextArea">
                    <?php echo $dbdata['email_details']; ?>
                </textarea>
                <p>Call</p>
                <input type="text" name="call_details" size="65" value="<?php echo $dbdata['call_details']; ?>" />
                <p>Fax</p>
                <input type="text" name="fax_details" size="65" value="<?php echo $dbdata['fax_details']; ?>" />
                <p>Business Hour Details</p>
                <textarea name="business_hours_details" class="customTextArea">
                    <?php echo $dbdata['business_hours_details']; ?>
                </textarea>
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>