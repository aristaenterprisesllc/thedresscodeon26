<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>:::The Dress Code On 26:::</title>
    <link type="text/css" href="<?php echo CSS_PATH; ?>style.css" rel="stylesheet" />
    <!-- <link type="text/css" href="<?php echo CSS_PATH; ?>custom_fonts.css" rel="stylesheet" /> -->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>ie7.css"><![endif]-->
	<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery-1.9.1.min.js"></script>
	<!-- bxSlider Javascript file -->
	<script src="<?php echo JS_PATH; ?>jquery.bxslider.js"></script>
	<!-- bxSlider CSS file -->
	<link type="text/css" href="<?php echo CSS_PATH; ?>jquery.bxslider.css" rel="stylesheet" />
	<script src="<?php echo JS_PATH; ?>jquery.bxslider.js"></script>	
	<!-- Tiny MCE -->
    <script type="text/javascript" src="<?php echo JS_PATH; ?>tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
		tinymce.init({
			
		    selector: "textarea",theme: "modern",
		    width: 600,
		    height: 200,
		    content_css : "<?php echo CSS_PATH; ?>custom_fonts.css",
		    plugins: [
		              "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
		              "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		              "save table contextmenu directionality emoticons template paste textcolor"
		        ],
		    toolbar: "insertfile undo redo | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
		    font_formats: "Andale Mono=andale mono,times;"+
		        "Arial=arial,helvetica,sans-serif;"+
		        "Arial Black=arial black,avant garde;"+
		        "Book Antiqua=book antiqua,palatino;"+
		        "Comic Sans MS=comic sans ms,sans-serif;"+
		        "Courier New=courier new,courier;"+
		        "Edwardian Srcipt=Edwardian;"+
		        "Georgia=georgia,palatino;"+
		        "Helvetica=helvetica;"+
		        "Impact=impact,chicago;"+
		        "Symbol=symbol;"+
		        "Tahoma=tahoma,arial,helvetica,sans-serif;"+
		        "Terminal=terminal,monaco;"+
		        "Times New Roman=times new roman,times;"+
		        "Trebuchet MS=trebuchet ms,geneva;"+
		        "Verdana=verdana,geneva;"+
		        "Webdings=webdings;"+
		        "Wingdings=wingdings,zapf dingbats"
		 });
	</script>
</head>
<body>
    <div id="header">
	    <div>
	        <div id="logo"><img src="<?php echo IMAGES_PATH; ?>Logo-Wide.png" width="325" height="82"></div>
            <ul>
                <li><a href="<?php echo NAV_PATH; ?>welcome">Home</a></li>
                <li><a href="<?php echo NAV_PATH; ?>products">Products</a></li>
                <li><a href="<?php echo NAV_PATH; ?>services">Services</a></li>
                <li><a href="<?php echo NAV_PATH; ?>about">About</a></li>
                <li><a href="<?php echo NAV_PATH; ?>contact">Contact</a></li>
            </ul>
            <?php if($this->session->userdata('logged_in')) { ?>
                    <span class="login" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin/logout">Logout</a></span>
                    <span class="adminmenu" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin">Admin</a></span>
            <?php } else { ?>
                    <span class="login" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin">Login</a></span>
            <?php } ?>
            
        </div>
    </div>