<div id="body">
	<div id="featured">
		<h3>Products Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                
                <h2>Add New Product</h2>
                <?php echo form_open_multipart('admins/products_admin/insertProduct');?>
                <p>Product Name</p>
                <input type="text" name="name" size="65" />
                <p>Product Details</p>
                <textarea name="details" class="customTextArea"></textarea>
                <p>Image</p>
                <input type="file" name="image" size="50"/><label>Only JPG is Allowed with 258px X 348px</label>
                <br/>
                <input type="submit" name="confirm" value="Save"/>
                
                <h2>Existing Products</h2>
                <table align="center" width="800px" cellspacing="10">
                    <tr>
                        <td>Thumbnails</td>
                        <td>Name</td>
                        <td>Details</td>
                        <td>Delete</td>
                        <td>Edit</td>
                    </tr>
                    <?php if(count($dbdata) > 0)
                        foreach($dbdata as $row) { ?>
                    <tr>
                        <td><img src="<?php echo IMAGES_PATH.$row['imageName']; ?>" width="70" height="80"></td>
                        <td><?php echo $row['name'];?></td>
                        <td><?php echo $row['details'];?></td>
                        <td>
                        	<!-- Delete Product -->
                        	<a href="<?php echo 'products_admin/deleteProduct/'.$row['id'].'/'.$row['imageName'];?>">
                        		<img src="<?php echo IMAGES_PATH; ?>btnDelete.png" >
                        	</a>
                        </td>
                        <td>
                        	<!-- Edit Product -->
                        	<a href="<?php echo 'products_admin/updateView/'.$row['id'];?>">
                        		<img src="<?php echo IMAGES_PATH; ?>btnEdit.png" >
                        	</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
		</div>
	</div>
</div>