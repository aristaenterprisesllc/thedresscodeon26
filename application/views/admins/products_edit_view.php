<div id="body">
	<div id="featured">
		<h3>Products Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
       		<p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                
            <h2>Update Product</h2>
            <?php echo form_open_multipart('admins/products_admin/updateProduct/'.$dbdata['id']);?>
            <p>Product Name</p>
            <input type="text" name="name" size="65" value="<?php echo $dbdata['name']; ?>" />
            <p>Product Details</p>
            <textarea name="details" class="customTextArea"><?php echo $dbdata['details']; ?></textarea>
            <p>Image</p>
            <img src="<?php echo IMAGES_PATH.$dbdata['imageName']; ?>" width="70" height="80">
            <input type="file" name="image" size="50"/><label>Only JPG is Allowed with 258px X 348px </label>
            <br/>
            <input type="submit" name="update" value="Update"/>
            <?php form_close()?>
		</div>
	</div>
</div>