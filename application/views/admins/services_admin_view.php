<div id="body">
	<div id="featured">
		<h3>Services Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/services_admin/updateValue/'); ?>
                <p>Title</p>
                <input type="text" name="title" size="65" value="<?php echo $dbdata['title']; ?>" />
                <p>Details</p>
                <textarea name="details" class="customTextArea">
                    <?php echo $dbdata['details']; ?>
                </textarea>
                <p>Middle Left Title</p>
                <input type="text" name="middle_left_title" size="65" value="<?php echo $dbdata['middle_left_title']; ?>" />
                <p>Middle Left Details</p>
                <textarea name="middle_left_details" class="customTextArea">
                    <?php echo $dbdata['middle_left_details']; ?>
                </textarea>
                <p>Middle Center Title</p>
                <input type="text" name="middle_center_title" size="65" value="<?php echo $dbdata['middle_center_title']; ?>" />
                <p>Middle Center Details</p>
                <textarea name="middle_center_details" class="customTextArea">
                    <?php echo $dbdata['middle_center_details']; ?>
                </textarea>
                <p>Middle Right Title</p>
                <input type="text" name="middle_right_title" size="65" value="<?php echo $dbdata['middle_right_title']; ?>" />
                <p>Middle Right Details</p>
                <textarea name="middle_right_details" class="customTextArea">
                    <?php echo $dbdata['middle_right_details']; ?>
                </textarea>
                <p>Bottom Text</p>
                <textarea name="bottom_details" class="customTextArea">
                    <?php echo $dbdata['bottom_details']; ?>
                </textarea>
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
                
                <br/><br/>
                <!-- Banner Image Upload -->
                <label>Banner Image</label>
                <?php echo form_open_multipart('admins/services_admin/uploadImage/banner_image'); ?>
	                <img src="<?php echo IMAGES_PATH.$dbdata['banner_image']; ?>" width="100" height="65">
	            	<input type="file" name="banner_image" size="50"/><label>Only JPG is Allowed with 370px X 260px</label><br/>
	            	<input type="submit" name="upload" value="Upload"/>
                <?php echo form_close(); ?>
                <br/><br/>
                <!-- Middle Left Image Upload -->
                <label>Middle Left Image</label>
                <?php echo form_open_multipart('admins/services_admin/uploadImage/middle_left_image'); ?>
	                <img src="<?php echo IMAGES_PATH.$dbdata['middle_left_image']; ?>" width="80" height="75">
	            	<input type="file" name="middle_left_image" size="50"/><label>Only JPG is Allowed with 180px X 170px</label><br/>
	            	<input type="submit" name="upload" value="Upload"/>
                <?php echo form_close(); ?>
                <br/><br/>
                <!-- Middle Center Image Upload -->
                <label>Middle Center Image</label>
                <?php echo form_open_multipart('admins/services_admin/uploadImage/middle_center_image'); ?>
	                <img src="<?php echo IMAGES_PATH.$dbdata['middle_center_image']; ?>" width="80" height="75">
	            	<input type="file" name="middle_center_image" size="50"/><label>Only JPG is Allowed with 180px X 170px</label><br/>
	            	<input type="submit" name="upload" value="Upload"/>
                <?php echo form_close(); ?>
                <br/><br/>
                <!-- MIddle Right Image Upload -->
                <label>Middle Right Image</label>
                <?php echo form_open_multipart('admins/services_admin/uploadImage/middle_right_image'); ?>
	                <img src="<?php echo IMAGES_PATH.$dbdata['middle_right_image']; ?>" width="80" height="75">
	            	<input type="file" name="middle_right_image" size="50"/><label>Only JPG is Allowed with 180px X 170px</label><br/>
	            	<input type="submit" name="upload" value="Upload"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>