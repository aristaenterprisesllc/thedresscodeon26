<div id="body">
	<div id="featured">
		<h3>Slide Show Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                
                <!-- Center Slide -->
                <h2>Center Slide</h2>
                <p>Add new image</p>
                <?php echo form_open_multipart('admins/slideshow_admin/insertImage/centerSlide');?>
                <input type="file" name="slideShow" size="50"/><label>Only JPG is Allowed with 318px X 499px</label>
                <br/>
                <input type="submit" name="upload" value="Upload"/>
                
                <table align="center" width="400px">
                    <tr>
                        <td>Thumbnails</td>
                        <td>File Name</td>
                        <td>Actions</td>
                    </tr>
                    <?php if(count($centerSlide) > 0)
                        foreach($centerSlide as $row) { ?>
                    <tr>
                        <td><img src="<?php echo IMAGES_PATH.$row['fileName']; ?>" width="70" height="50"></td>
                        <td><?php echo $row['fileName'];?></td>
                        <td><a href="<?php echo 'slideshow_admin/deleteImage/'.$row['id'].'/'.$row['fileName']; ?>">
                        		<img src="<?php echo IMAGES_PATH; ?>btnDelete.png" >
                        	</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo form_close(); ?>
                <!-- Right Top Slide -->
                <h2>Right Top Slide</h2>
                <p>Add new image</p>
                <?php echo form_open_multipart('admins/slideshow_admin/insertImage/rightTopSlide');?>
                <input type="file" name="slideShow" size="50"/><label>Only JPG is Allowed with 240px X 170px</label>
                <br/>
                <input type="submit" name="upload" value="Upload"/>
                
                <table align="center" width="400px">
                    <tr>
                        <td>Thumbnails</td>
                        <td>File Name</td>
                        <td>Actions</td>
                    </tr>
                    <?php if(count($rightTopSlide) > 0)
                        foreach($rightTopSlide as $row) { ?>
                    <tr>
                        <td><img src="<?php echo IMAGES_PATH.$row['fileName']; ?>" width="70" height="50"></td>
                        <td><?php echo $row['fileName'];?></td>
                        <td><a href="<?php echo 'slideshow_admin/deleteImage/'.$row['id'].'/'.$row['fileName']; ?>">
                        		<img src="<?php echo IMAGES_PATH; ?>btnDelete.png" >
                       		</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo form_close(); ?>
                <!-- Right Bottom Slide -->
                <h2>Right Bottom Slide</h2>
                <p>Add new image</p>
                <?php echo form_open_multipart('admins/slideshow_admin/insertImage/rightBottomSlide');?>
                <input type="file" name="slideShow" size="50"/><label>Only JPG is Allowed with 240px X 170px</label>
                <br/>
                <input type="submit" name="upload" value="Upload"/>
                
                <table align="center" width="400px">
                    <tr>
                        <td>Thumbnails</td>
                        <td>File Name</td>
                        <td>Actions</td>
                    </tr>
                    <?php if(count($rightBottomSlide) > 0)
                        foreach($rightBottomSlide as $row) { ?>
                    <tr>
                        <td><img src="<?php echo IMAGES_PATH.$row['fileName']; ?>" width="70" height="50"></td>
                        <td><?php echo $row['fileName'];?></td>
                        <td><a href="<?php echo 'slideshow_admin/deleteImage/'.$row['id'].'/'.$row['fileName']; ?>">
                        		<img src="<?php echo IMAGES_PATH; ?>btnDelete.png" >
                        	</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>