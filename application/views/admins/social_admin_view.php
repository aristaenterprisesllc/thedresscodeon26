<div id="body">
	<div id="featured">
		<h3>Social Link Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/social_admin/updateValue/'); ?>
                <p>Google Plus</p>
                <input type="text" name="google" size="65" value="<?php echo $social['google']; ?>" />
                <p>Twitter</p>
                <input type="text" name="twitter" size="65" value="<?php echo $social['twitter']; ?>" />
                <p>Facebook</p>
                <input type="text" name="facebook" size="65" value="<?php echo $social['facebook']; ?>" />
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>