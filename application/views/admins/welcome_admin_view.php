<div id="body">
	<div id="featured">
		<h3>Welcome Configuration</h3>
	</div>
	<div id="content">
		<div id="about">
                <p class="dbSaveMsg"><?php echo $this->session->flashdata('message'); ?></p>
                <?php echo form_open('admins/welcome_admin/updateValue/'); ?>
                <p>Left Box Title</p>
                <input type="text" name="left_title" size="65" value="<?php echo $dbdata['left_title']; ?>" />
                <p>Welcome Text</p>
                <textarea name="left_details" class="customTextArea">
                    <?php echo $dbdata['left_details']; ?>
                </textarea>
                <p>Right Top Box Text</p>
                <input type="text" name="right_details" size="65" value="<?php echo $dbdata['right_details']; ?>" />
                <p>Middle Box Title</p>
                <input type="text" name="middle_title" size="65" value="<?php echo $dbdata['middle_title']; ?>" />
                <br/><br/>
                <input type="submit" name="confirm" value="Save"/>
                <?php echo form_close(); ?>
		</div>
	</div>
</div>