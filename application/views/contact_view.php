<div id="body">
    <div id="featured">
            <h3><?php echo $dbdata['title']; ?></h3>
    </div>
    <div id="content">    
        <div id="contact">
            <p><?php echo $dbdata['subtitle']; ?></p>
            <div class="contactInfo">
                <div>
                    <h3>Location</h3>
                    <p>
                        <?php echo $dbdata['location_details']; ?>
                    </p>
                    <h3>Email</h3>
                    <p>
                        <?php echo $dbdata['email_details']; ?>
                    </p>
                    <h3>Call</h3>
                    <p>
                        <?php echo $dbdata['call_details']; ?>
                    </p>
                    <h3>Fax</h3>
                    <p>
                        <?php echo $dbdata['fax_details']; ?>
                    </p>
                    <h3>Bussiness hours</h3>
                    <p>
                        <?php echo $dbdata['business_hours_details']; ?>
                    </p>
                    <h3>Map</h3>
                    <div id="map_canvas" style="width: 300px; height:300px;"></div>
                </div>
                <form action="#">
                    <h3>Send a Message</h3>
                    <label for="name"><span>Your Name:</span>
                        <input type="text" id="name">
                    </label>
                    <label for="email"><span>Email Address:</span>
                        <input type="text" id="email">
                    </label>
                    <label for="message"><span class="message">Message:</span>
                        <textarea name="message" id="message" cols="30" rows="10"></textarea>
                    </label>
                    <input type="submit" id="submit" value="Send message">
                </form>
            </div>
        </div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>

function initialize() {
	var usa = new google.maps.LatLng(34.047252,-118.490776);
    var mapOptions = {
    	zoom: 16,
        center: usa,
        mapTypeId: google.maps.MapTypeId.ROADMAP
   	}
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    var marker = new google.maps.Marker({
        position: usa,
        title:"250 26th St # 101 Santa Monica, CA 90402 USA"
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>