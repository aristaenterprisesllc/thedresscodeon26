    <div id="footer">
        <div>
            <div class="first">
                <h4><a href="<?php echo NAV_PATH ?>welcome">Home</a> | 
                    <a href="<?php echo NAV_PATH ?>products">Products</a> | 
                    <a href="<?php echo NAV_PATH ?>services">Services</a> | 
                    <a href="<?php echo NAV_PATH ?>about">About</a> | 
                    <a href="<?php echo NAV_PATH ?>contact">Contact</a></h4>
            </div>
            <div class="last">
                <div>
                    <a href="<?php echo $social['facebook']; ?>"><img src="<?php echo IMAGES_PATH; ?>facebook.png" width="45px" height="45px" /></a>
                    <a href="<?php echo $social['twitter']; ?>"><img src="<?php echo IMAGES_PATH; ?>twitter.png" width="45px" height="45px" /></a>
                    <a href="<?php echo $social['google']; ?>"><img src="<?php echo IMAGES_PATH; ?>gplus.png" width="45px" height="45px" /></a>
                </div>
            </div>
        </div>
        <p class="footnote">&copy; Copyright 2013. All rights reserved | Developed by <a href="http://www.aristaenterprise.com" target="_blank">Arista Enterprises, LLC.</a></p>
    </div>
</body>
</html>