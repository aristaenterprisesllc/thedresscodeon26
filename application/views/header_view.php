<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>:::The Dress Code On 26:::</title>
    <link type="text/css" href="<?php echo CSS_PATH; ?>style.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo CSS_PATH; ?>custom_fonts.css" rel="stylesheet" />
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>ie7.css"><![endif]-->
	<script type="text/javascript" src="<?php echo JS_PATH; ?>jquery-1.9.1.min.js"></script>
	<!-- bxSlider Javascript file -->
	<script src="<?php echo JS_PATH; ?>jquery.bxslider.js"></script>
	<!-- bxSlider CSS file -->
	<link type="text/css" href="<?php echo CSS_PATH; ?>jquery.bxslider.css" rel="stylesheet" />
	<script src="<?php echo JS_PATH; ?>jquery.bxslider.js"></script>	
</head>
<body <?php if ($current == "contact") { ?>onload="initialize()"<?php } ?>>
    <div id="header">
	    <div>
	        <div id="logo"><img src="<?php echo IMAGES_PATH; ?>Logo-Wide.png" width="325" height="82"></div>
            <ul>
                <li><a href="<?php echo NAV_PATH ?>welcome" <?php if ($current == "home") { ?>class="current"<?php } ?> >Home</a></li>
                <li><a href="<?php echo NAV_PATH ?>products" <?php if ($current == "products") { ?>class="current"<?php } ?> >Products</a></li>
                <li><a href="<?php echo NAV_PATH ?>services" <?php if ($current == "services") { ?>class="current"<?php } ?> >Services</a></li>
                <li><a href="<?php echo NAV_PATH ?>about" <?php if ($current == "about") { ?>class="current"<?php } ?> >About</a></li>
                <li><a href="<?php echo NAV_PATH ?>contact" <?php if ($current == "contact") { ?>class="current"<?php } ?> >Contact</a></li>
            </ul>
            <?php if($this->session->userdata('logged_in')) { ?>
                    <span class="login" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin/logout">Logout</a></span>
                    <span class="adminmenu" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin">Admin</a></span>
            <?php } else { ?>
                    <span class="login" align="right"><a href="<?php echo NAV_PATH ?>admins/login_admin">Login</a></span>
            <?php } ?>
        </div>
    </div>