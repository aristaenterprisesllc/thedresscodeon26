	<div id="body">
		<div id="featured">
			<!-- <h1>World&#39;s Finest and Elegant Tuxedo</h1> -->
			<h1><?php echo $title['left_title']; ?></h1>
		</div>
		<div id="content">
			<div id="product">
				<ul>
					<?php if(count($dbdata) > 0)
                       	foreach($dbdata as $row) { ?>
                           	<li>
                           		<img src="<?php echo IMAGES_PATH.$row['imageName']; ?>" width="258" height="348" alt="Image">
                               	<h2><?php echo $row['name']; ?></h2>
								<p>
									<?php echo $row['details']; ?>
								</p>
                           	</li>
                  	<?php } ?>
				</ul>
			</div>
		</div>
	</div>