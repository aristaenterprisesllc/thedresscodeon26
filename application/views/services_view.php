	<div id="body">
		<div id="featured">
			<div class="section">
				<img src="<?php echo IMAGES_PATH.$dbdata['banner_image']; ?>" alt="Image">
				<div>
					<h2><?php echo $dbdata['title']; ?></h2>
					<p><?php echo $dbdata['details']; ?></p>
				</div>
			</div>
		</div>
		<div id="content">
			<div id="services">
				<div class="section">
					<ul>
                    	<li>
                        	<img src="<?php echo IMAGES_PATH.$dbdata['middle_left_image']; ?>" width="180" height="170" alt="Image">
                            <h2><?php echo $dbdata['middle_left_title']; ?></h2>
                            <p><?php echo $dbdata['middle_left_details']; ?></p>
                      	</li>
                        <li>
                        	<img src="<?php echo IMAGES_PATH.$dbdata['middle_center_image']; ?>" width="180" height="170" alt="Image">
                  	        <h2><?php echo $dbdata['middle_center_title']; ?></h2>
                            <p><?php echo $dbdata['middle_center_details']; ?></p>
                        </li>
                        <li>
                            <img src="<?php echo IMAGES_PATH.$dbdata['middle_right_image']; ?>" width="180" height="170" alt="Image">
                            <h2><?php echo $dbdata['middle_right_title']; ?></h2>
                            <p><?php echo $dbdata['middle_right_details']; ?></p>
                        </li>
                   	</ul>
					<div>
                    	<?php echo $dbdata['bottom_details']; ?>
					</div>
				</div>
				<div class="sidebar">
	                <h3>For Appointment</h3>
                    <div class="first">
                    	 <span>Call:</span>
                         <p><?php echo $appointment['call_details']; ?></p>
                         <span>Fax:</span>
                         <p><?php echo $appointment['fax_details']; ?></p>
                   	</div>
                    <div>
                         <span>Email:</span>
                         <?php echo $appointment['email_details']; ?>
                    </div>
                    <div>
                         <span>Location:</span>
                         <p><?php echo $appointment['location_details']; ?></p>
                    </div>
				</div>
			</div>
		</div>
	</div>