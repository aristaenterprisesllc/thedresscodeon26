        <div id="body">
            <div id="featured">
                <div class="first">
                    <div class="note" style="width:250px;">
                        <h2><?php echo $dbdata['left_title']; ?></h2>
                        <p><?php echo $dbdata['left_details']; ?></p>
                        <a href="<?php echo NAV_PATH ?>products" id="shopnow">Shop Now</a>
                    </div>
                	<span style="width:318px; height:499px;">
                		<!-- <div class="bxslider" style="width:318px; height:auto;"> -->
  							<?php if(count($centerSlide) < 2) { ?>
                                    <img src="<?php echo IMAGES_PATH.$centerSlide[0]['fileName']; ?>"  width="318" height="499" />
                           	<?php } else if(count($centerSlide) > 1){ ?>
                           		<div class="bxslider" style="width:318px; height:auto;">
                                   <?php foreach($centerSlide as $row) { ?>
                                    	<img src="<?php echo IMAGES_PATH.$row['fileName']; ?>"  width="318" height="499" />
                                    <?php } ?>
                              	</div>
                           	<?php } ?>
						<!-- </div> -->
                	</span>
                </div>
                
                <div class="last">
                    <h3 style="font-size:19px; padding-bottom:3px;"><?php echo $dbdata['right_details']; ?></h3>
                    
                    	<?php if(count($rightTopSlide) < 2) { ?>
                           	<ul>
                            	<li>
                                  	<a href="<?php echo NAV_PATH ?>products">
                                   		<img src="<?php echo IMAGES_PATH.$rightTopSlide[0]['fileName']; ?>"  width="240" height="170" />
                               		</a>
                               	</li>
                          	</ul>
                    	<?php } else if(count($rightTopSlide) > 1) { ?>
                    		<ul class="bxslider2">
                        	<?php foreach($rightTopSlide as $row) { ?>
                            	<li>
                                  	<a href="<?php echo NAV_PATH ?>products">
                                   		<img src="<?php echo IMAGES_PATH.$row['fileName']; ?>"  width="240" height="170" />
                               		</a>
                               	</li>
                               	<?php } ?>
                           	</ul>
                        <?php } ?>
                    
                    <p>&nbsp;<!-- This is just a place holder --></p>
                    <?php if(count($rightBottomSlide) < 2) { ?>
                    		<ul>
                            	<li>
                                  	<a href="<?php echo NAV_PATH ?>products">
                                   		<img src="<?php echo IMAGES_PATH.$rightBottomSlide[0]['fileName']; ?>"  width="240" height="170" />
                               		</a>
                               	</li>
                         	</ul>
                    	<?php } else if(count($rightBottomSlide) > 1) { ?>
                    		<ul class="bxslider3">
                        	<?php foreach($rightBottomSlide as $row) { ?>
                            	<li>
                                  	<a href="<?php echo NAV_PATH ?>products">
                                   		<img src="<?php echo IMAGES_PATH.$row['fileName']; ?>"  width="240" height="170" />
                               		</a>
                               	</li>
                               	<?php } ?>
                           	</ul>
                        <?php } ?>
                    
                    <p>&nbsp;<!-- This is just a place holder --></p>
                </div>
            </div>
            <div id="content">
                <div id="home">
                    <div class="sidebar">
                        <h3>For Appointment</h3>
                        <div class="first">
                            <span>Call:</span>
                            <p>
                            	<?php echo $appointment['call_details']; ?>
                            </p>
                            <span>Fax:</span>
                            <p>
                            	<?php echo $appointment['fax_details']; ?>
                            </p>
                        </div>
                        <div>
                            <span>Email:</span>
                            <?php echo $appointment['email_details']; ?>
                        </div>
                        <div>
                            <span>Location:</span>
                            <p>
                            	<?php echo $appointment['location_details']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="section">
                        <!--<h4>About</h4>-->
                        <h1><?php echo $dbdata['middle_title']; ?></h1>
                        <ul>
                            <li>
                                <img src="<?php echo IMAGES_PATH.$services['middle_left_image']; ?>" width="180" height="170" alt="Image">
                                <h2><?php echo $services['middle_left_title']; ?></h2>
                                <p><?php echo $services['middle_left_details']; ?></p>
                            </li>
                            <li>
                                <img src="<?php echo IMAGES_PATH.$services['middle_center_image']; ?>" width="180" height="170" alt="Image">
                                <h2><?php echo $services['middle_center_title']; ?></h2>
                                <p><?php echo $services['middle_center_details']; ?></p>
                            </li>
                            <li>
                                <img src="<?php echo IMAGES_PATH.$services['middle_right_image']; ?>" width="180" height="170" alt="Image">
                                <h2><?php echo $services['middle_right_title']; ?></h2>
                                <p><?php echo $services['middle_right_details']; ?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
	</div>
	
	<style type="text/css">
    	/* BX Slider Style */

		.bx-wrapper .bx-viewport
		{
			border:none;
			background:none;
			box-shadow:none;
		}
						
		.bxslider
		{
			margin:0;
			padding:0;
		}
   	</style>
	
	<script type="text/javascript">
	 	$(document).ready(function(){
			
	     	$('.bxslider').bxSlider({
	     		auto: true,
	  			autoControls: false,
	  			pager:false,
	  			controls:false,
				mode:'fade',
				speed:1800
	     	});
	     	$('.bxslider2').bxSlider({
	     		auto: true,
	  			autoControls: false,
	  			pager:false,
	  			controls:false,
	  			mode:'fade',
				speed:1000
	     	});
	     	$('.bxslider3').bxSlider({
	     		auto: true,
	  			autoControls: false,
	  			pager:false,
	  			controls:false,
	  			mode:'fade',
	  			speed:1000
	     	});
     	});
	 </script>