-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 11:45 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thedresscodedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `top_title` varchar(250) NOT NULL,
  `top_details` text NOT NULL,
  `middle_title` varchar(250) NOT NULL,
  `middle_details` text NOT NULL,
  `bottom_title` varchar(250) NOT NULL,
  `bottom_details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `top_title`, `top_details`, `middle_title`, `middle_details`, `bottom_title`, `bottom_details`) VALUES
(1, 'Fine tailoring so you can look your best!', 'Who we are!!!', '<p><span style="font-family: georgia,palatino; font-size: large;"> We are a family run business specializing in Custom made clothing and we offer expert tailoring. Our Tailors have years of experience. Our fabric selection ranges from Loro Piana cashmere and shirting to Dormeuil and Zegna. Our clientele is as variable as our fabric selection, we don''t have a "typical" client and that is what makes The Dress Code so unique. </span></p>', 'What we do!!!', '<p><span style="font-family: georgia,palatino; font-size: medium;"> Here at The Dress Code we offer more than just Custom Designs, we also offer alterations and repairs. We don''t look like your everyday tailor shop. We have a modest selection of ready-to-wear clothing. We carry jeans from Red Engine and basic Tees from Majestic, and our custom designs are always on display as well. We are sort of a one stop shop. You can buy a pair of jeans and get them altered in the same place. </span></p>', 'About Diego', '<p><span style="font-family: georgia,palatino;"><span style="font-size: medium;"> Owner and Master Tailor, Diego Astorga, has over 30 years of experience. . "I have been a tailor for 37 years now and I specialize in making Custom Made clothing for men and women. It is always a thrill to work with people and create something unique for them. Whether it''s a gown or a sundress, a suit or a sport jacket, the possibilities are endless. I take pride in giving every garment certain details that make it unique for its owner."</span> </span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE IF NOT EXISTS `appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_details` varchar(250) NOT NULL,
  `fax_details` varchar(250) NOT NULL,
  `email_details` varchar(250) NOT NULL,
  `location_details` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `call_details`, `fax_details`, `email_details`, `location_details`) VALUES
(1, '(310) 656-9884/444', '(310) 656-7884/333', '<p><span style="font-size: small;"><span style="font-family: arial,helvetica,sans-serif;"> <a class="email" href="mailto:thedresscodeon26th@yahoo.com">thedresscodeon26@yahoo.com</a> </span></span></p>', '<p><span style="font-family: courier new,courier; font-size: medium;"><em>250 Twenty Sixth Street<br />Suite 101<br />Santa Monica, CA 90402, U.S.A</em></span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `subtitle` text NOT NULL,
  `location_details` text NOT NULL,
  `email_details` varchar(250) NOT NULL,
  `call_details` varchar(250) NOT NULL,
  `fax_details` varchar(250) NOT NULL,
  `business_hours_details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `subtitle`, `location_details`, `email_details`, `call_details`, `fax_details`, `business_hours_details`) VALUES
(1, 'Home visit appointments are Mondays only. Please call for appointment and Fee.', '<p><span style="font-family: andale mono,times; font-size: medium;">*15 pieces or more please call ahead. Otherwise no appointments are necessary for in store visits.</span></p>', '<p><span style="font-family: times new roman,times; font-size: medium;">250 twenty sixth street</span><br /><span style="font-family: times new roman,times; font-size: medium;">suite 101</span><br /><span style="font-family: times new roman,times; font-size: medium;">santa monica,&nbsp;CA 90402</span></p>', '<p><span style="font-family: times new roman,times; font-size: medium;">thedresscodeon26.yahoo.com</span></p>', '(310) 656-9884', '(310) 656-7884', '<p><span style="font-family: times new roman,times; font-size: medium;">tues - fri 9:00 am to 6:00 pm</span><br /><span style="font-family: times new roman,times; font-size: medium;">sat 9:00 am to 5:00 pm</span><br /><span style="font-family: times new roman,times; font-size: medium;">monday and sunday closed</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `left_title` varchar(250) NOT NULL,
  `left_details` text NOT NULL,
  `right_details` varchar(250) NOT NULL,
  `middle_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `left_title`, `left_details`, `right_details`, `middle_title`) VALUES
(1, 'World''s Finest and Elegant Tuxedo', '<p><span style="font-family: times new roman,times; font-size: medium;">This is just a place holder, so you can see what the site would look like. This is just a place holder, so you can see what the site would look like.</span></p>', 'Handcrafted and made from 100% Wool', 'We are Makers of Custom Handmade Tailored Suits');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `details` text NOT NULL,
  `imageName` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `details`, `imageName`) VALUES
(1, 'PRODUCT NAME', '<p>This is just a place holder</p>', 'product-1.jpg'),
(2, 'PRODUCT NAME', 'This is just a place holder', 'product-5.jpg'),
(3, 'PRODUCT NAME', 'This is just a place holder', 'product-6.jpg'),
(4, 'PRODUCT NAME', 'This is just a place holder', 'product-3.jpg'),
(10, 'Test', '<p><em><span style="font-family: arial,helvetica,sans-serif; font-size: medium;">This is just a place holder</span></em></p>', '2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `details` text NOT NULL,
  `banner_image` varchar(250) NOT NULL,
  `middle_left_title` varchar(250) NOT NULL,
  `middle_left_details` text NOT NULL,
  `middle_left_image` varchar(250) NOT NULL,
  `middle_center_title` varchar(250) NOT NULL,
  `middle_center_details` text NOT NULL,
  `middle_center_image` varchar(250) NOT NULL,
  `middle_right_title` varchar(250) NOT NULL,
  `middle_right_details` text NOT NULL,
  `middle_right_image` varchar(250) NOT NULL,
  `bottom_details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `details`, `banner_image`, `middle_left_title`, `middle_left_details`, `middle_left_image`, `middle_center_title`, `middle_center_details`, `middle_center_image`, `middle_right_title`, `middle_right_details`, `middle_right_image`, `bottom_details`) VALUES
(1, 'O u r s e r v i c e s i n c l u d e :', '<ul>\n<li><span style="font-family: book antiqua,palatino; font-size: medium;"><em>Custom Made Clothing for Men and Women</em></span></li>\n<li><span style="font-family: book antiqua,palatino; font-size: medium;"><em>Custom Design</em></span></li>\n<li><span style="font-family: book antiqua,palatino; font-size: medium;"><em>Expert Alterations and Repairs</em></span></li>\n<li><span style="font-family: book antiqua,palatino; font-size: medium;"><em>Reweaving</em></span></li>\n<li><span style="font-family: book antiqua,palatino; font-size: medium;"><em>For Home or Office Visits Please call for an appointment</em></span></li>\n</ul>', 'perfectfit2.jpg', 'HANDMADE', '<p><span style="font-family: courier new,courier; font-size: medium;"><em>This is just a place holder, so you can see what the site would look like</em></span></p>', 'service-1.jpg', 'DESIGN', '<p><span style="font-size: medium;"><strong><span style="font-family: georgia,palatino;">This is just a place holder, so you can see what the site would look like</span></strong></span></p>', 'service-2.jpg', 'PERFECT FIT', '<p><span style="font-family: trebuchet ms,geneva; font-size: medium;">This is just a place holder, so you can see what the site would look like</span></p>', 'service-3.jpg', '<p><span style="font-family: terminal,monaco; font-size: large;">If you are not sure if we can work on something please call or come in and we can take a look and tell you if it is possible without ruining your garment</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE IF NOT EXISTS `slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(250) NOT NULL,
  `slideName` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `fileName`, `slideName`) VALUES
(1, 'suit1.jpg', 'centerSlide'),
(2, '4B.jpg', 'centerSlide'),
(3, '1.jpg', 'centerSlide'),
(14, '2B.jpg', 'centerSlide'),
(5, '3B.jpg', 'centerSlide'),
(6, '2.jpg', 'rightTopSlide'),
(7, 'im1.jpg', 'rightTopSlide'),
(8, 'im2.jpg', 'rightTopSlide'),
(9, 'im3.jpg', 'rightTopSlide'),
(10, 'ladies-dress.jpg', 'rightBottomSlide'),
(11, 'im4.jpg', 'rightBottomSlide'),
(12, 'im5.jpg', 'rightBottomSlide'),
(13, 'im6.jpg', 'rightBottomSlide'),
(15, 'l_16fem.jpg', 'rightBottomSlide'),
(16, 'l_suit1.jpg', 'rightTopSlide'),
(17, 'templatemo_image_01.jpg', 'rightTopSlide');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_pass`, `user_date`, `user_modified`, `user_last_login`) VALUES
(3, 'admin', '$P$BgePJkpNkTnRcvW/vQ3u4gFZVo1cP9/', '2013-04-08 03:43:52', '2013-04-08 03:43:52', '2013-11-04 13:42:42');
